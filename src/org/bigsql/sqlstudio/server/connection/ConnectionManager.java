/*
 * SQL Studio
 * 
 * Copyright (c) 2014, BigSQL.
 * Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Copyright (c) 2012 - 2013, StormDB, Inc.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement is
 * hereby granted, provided that the above copyright notice and this paragraph and
 * the following two paragraphs appear in all copies.
 * 
 * IN NO EVENT SHALL OPEN SOURCE CONSULTING GROUP BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 * OPEN SOURCE CONSULTING GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * OPEN SOURCE CONSULTING GROUP SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND
 * OPEN SOURCE CONSULTING GROUP HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 * 
 */
package org.bigsql.sqlstudio.server.connection;

import java.sql.Connection;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.bigsql.sqlstudio.server.util.DatabaseTypes;
import org.bigsql.sqlstudio.shared.DatabaseConnectionException;

/**
 * 
 * <pre>
 * Java File.
 * Title        : ConnectionManager.java
 * Description  : Connection Manager interface
 * @author      : Chalimadugu Ranga Reddy
 * @version     : 1.0 13-Oct-2014
 * </pre>
 */
public final class ConnectionManager
{

	/*
	 * Define Logger.
	 */
	static final Logger logger = Logger.getLogger(ConnectionManager.class
			.getName());

	/*
	 * Don't let anyone instantiate this class
	 */
	private ConnectionManager()
	{}

	/**
	 * Method to connection with selected option
	 * 
	 * @param requestArg
	 *            HttpServletRequest object
	 * @param token
	 *            Hash table token key to retrieve stored connection
	 * @param clientIP
	 *            Client Machine IP address
	 * @param userAgent
	 *            User agent info
	 * @return Connection Object
	 * @throws DatabaseConnectionException
	 *             If any by calling database API
	 */
	public static Connection getConnection(HttpServletRequest requestArg,
			String token, String clientIP, String userAgent)
			throws DatabaseConnectionException
	{
		Connection con = null;

		String database = (String) requestArg.getSession(false).getAttribute(
				"database-type");

		switch (DatabaseTypes.valueOf(database))
		{
			case PostgreSQL:
				con = PostgreSQLConnection.getInstance().getConnection(token,
						clientIP, userAgent);
				break;
			case Cassandra:
				con = CassandraConnection.getInstance().getConnection(token,
						clientIP, userAgent);
				break;
			default:
				System.out
						.printf("getConnection: Sorry, Supplied database <%s> hasn't configured in our enumerations.",
								database);
				break;
		}

		return con;
	}

	/**
	 * Method to create physical connection with selected database type and
	 * stores into hash table for future retrievals
	 * 
	 * @param requestArg
	 *            HttpServletRequest object
	 * @param databaseURL
	 *            Database Host name/ip details
	 * @param databasePort
	 *            Database Port
	 * @param databaseName
	 *            Database Name
	 * @param user
	 *            Database user to connect
	 * @param password
	 *            Database password to connect
	 * @param clientIP
	 *            IP address of the app running machine/device
	 * @param userAgent
	 *            User agent info
	 * @return Token info of database connection persisted in hash table
	 * @throws DatabaseConnectionException
	 *             If any by calling database API
	 */
	public static String addConnection(HttpServletRequest requestArg,
			String databaseURL, int databasePort, String databaseName,
			String user, String password, String clientIP, String userAgent)
			throws DatabaseConnectionException
	{
		String dbToken = null;
		String database = (String) requestArg.getSession(false).getAttribute(
				"database-type");

		switch (DatabaseTypes.valueOf(database))
		{
			case PostgreSQL:
				dbToken = PostgreSQLConnection.getInstance().addConnection(
						databaseURL, databasePort, databaseName, user,
						password, clientIP, userAgent);
				break;
			case Cassandra:
				dbToken = CassandraConnection.getInstance().addConnection(
						databaseURL, databasePort, databaseName, user,
						password, clientIP, userAgent);
				break;
			default:
				System.out
						.printf("addConnection: Sorry, Supplied database <%s> hasn't configured in our enumerations.",
								database);
				break;
		}

		return dbToken;
	}

	/**
	 * Method to release database connection
	 * 
	 * @param requestArg
	 *            HttpServletRequest object
	 * @param token
	 *            Hash table token key to retrieve stored connection
	 * @param clientIP
	 *            P address of the app running machine/device
	 * @param userAgent
	 *            User agent info
	 * @throws DatabaseConnectionException
	 *             If any by calling database API
	 */
	public static void closeConnection(HttpServletRequest requestArg,
			String token, String clientIP, String userAgent)
			throws DatabaseConnectionException
	{
		if (requestArg.getSession(false) != null)
		{

			String database = (String) requestArg.getSession(false)
					.getAttribute("database-type");

			switch (DatabaseTypes.valueOf(database))
			{
				case PostgreSQL:
					PostgreSQLConnection.getInstance().closeConnection(token,
							clientIP, userAgent);
					break;
				case Cassandra:
					CassandraConnection.getInstance().closeConnection(token,
							clientIP, userAgent);
					break;
				default:
					System.out
							.printf("closeConnection: Sorry, Supplied database <%s> hasn't configured in our enumerations.",
									database);
					break;
			}
		}

	}

	/**
	 * Method to get Database Version
	 * 
	 * @param requestArg
	 *            HttpServletRequest Object
	 * @return database version, if supported by underlying database
	 */
	public static int getDatabaseVersion(HttpServletRequest requestArg)
	{
		int dbVersion = -1;
		String database = (String) requestArg.getSession(false).getAttribute(
				"database-type");

		switch (DatabaseTypes.valueOf(database))
		{
			case PostgreSQL:
				dbVersion = PostgreSQLConnection.getInstance()
						.getDatabaseVersion();
				break;
			case Cassandra:
				dbVersion = CassandraConnection.getInstance()
						.getDatabaseVersion();
				break;
			default:
				System.out
						.printf("getDatabaseVersion: Sorry, Supplied database <%s> hasn't configured in our enumerations.",
								database);
				break;
		}

		return dbVersion;
	}
}
