/*
 * SQL Studio
 * 
 * Copyright (c) 2014, BigSQL.
 * Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Copyright (c) 2012 - 2013, StormDB, Inc.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement is
 * hereby granted, provided that the above copyright notice and this paragraph and
 * the following two paragraphs appear in all copies.
 * 
 * IN NO EVENT SHALL OPEN SOURCE CONSULTING GROUP BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 * OPEN SOURCE CONSULTING GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * OPEN SOURCE CONSULTING GROUP SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND
 * OPEN SOURCE CONSULTING GROUP HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 * 
 */
package org.bigsql.sqlstudio.server.connection;

import java.sql.Connection;

import org.bigsql.sqlstudio.shared.DatabaseConnectionException;

/**
 * <pre>
 * Java File.
 * Title        : DatabaseConnection.java
 * Description  : Database Connection Interface to define supported operations
 * @author      : Chalimadugu Ranga Reddy
 * @version     : 1.0 13-Oct-2014
 * </pre>
 */
public interface DatabaseConnection
{

	/**
	 * Method to fetch stored database connection
	 * 
	 * @param token
	 *            Hashtable token
	 * @param clientIP
	 *            Client IP address
	 * @param userAgent
	 *            User Agent
	 * @return Connection
	 * @throws DatabaseConnectionException
	 *             If any by API calls
	 */
	public Connection getConnection(String token, String clientIP,
			String userAgent) throws DatabaseConnectionException;

	/**
	 * Method to close database connection
	 * 
	 * @param token
	 *            Hashtable token
	 * @param clientIP
	 *            Client IP address
	 * @param userAgent
	 *            User Agent Info
	 * @throws DatabaseConnectionException
	 *             If any by API calls
	 */
	public void closeConnection(String token, String clientIP, String userAgent)
			throws DatabaseConnectionException;

	/**
	 * Method to create physical connection with underlying database
	 * 
	 * @param databaseURL
	 *            Database Host
	 * @param databasePort
	 *            Database Port
	 * @param databaseName
	 *            Database Name
	 * @param user
	 *            Connection User Name
	 * @param password
	 *            Connection Password
	 * @return Connection
	 * @throws DatabaseConnectionException
	 *             If any by API calls
	 */
	public Connection createConnection(String databaseURL, int databasePort,
			String databaseName, String user, String password)
			throws DatabaseConnectionException;

	/**
	 * Method creates physical connection with underlying database and adds to
	 * the hast table for future references
	 * 
	 * @param databaseURL
	 *            Database Host
	 * @param databasePort
	 *            Database Port
	 * @param databaseName
	 *            Database Name
	 * @param user
	 *            Connection User Name
	 * @param password
	 *            Connection Password
	 * @param clientIP
	 *            Client IP address
	 * @param userAgent
	 *            User Agent Info
	 * @return Acknowledges with token added to the hash table
	 * @throws DatabaseConnectionException
	 *             If any by API calls
	 */
	public String addConnection(String databaseURL, int databasePort,
			String databaseName, String user, String password, String clientIP,
			String userAgent) throws DatabaseConnectionException;

	/**
	 * Method to fetch version number of the underlying database
	 * 
	 * @return Database Version, If supported by given API's
	 */
	public int getDatabaseVersion();

}
