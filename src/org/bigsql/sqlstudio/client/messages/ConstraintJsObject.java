/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class ConstraintJsObject extends JavaScriptObject {

	protected ConstraintJsObject() {
    }


    public final native String getId() /*-{ return this.id}-*/;

    public final native String getConstraintName() /*-{ return this.name }-*/;
    public final native String getConstraintType() /*-{ return this.type }-*/;
    public final native String getDeferrable() /*-{ return this.deferrable }-*/;
    public final native String getDeferred() /*-{ return this.deferred }-*/;
    public final native String getIndexName() /*-{ return this.index_name }-*/;
    public final native String getIndexOwner() /*-{ return this.index_owner }-*/;    
    public final native String getUpdateAction() /*-{ return this.update_action }-*/;
    public final native String getDeleteAction() /*-{ return this.delete_action }-*/;    
    public final native String getDefinition() /*-{ return this.definition }-*/;

}
