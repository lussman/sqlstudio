/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class QueryErrorJsObject extends JavaScriptObject {

	protected QueryErrorJsObject() {
    }

    public final native String getError() /*-{ return this.error }-*/;

    public final native int getErrorCode() /*-{ return this.error_code }-*/;
}
