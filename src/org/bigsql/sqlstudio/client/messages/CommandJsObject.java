/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class CommandJsObject extends JavaScriptObject {

	protected CommandJsObject() {
    }

    public final native String getStatus() /*-{ return this.status }-*/;
    public final native String getMessage() /*-{ return this.message }-*/;

}
