/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.handlers;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.bigsql.sqlstudio.client.messages.CommandJsObject;
import org.bigsql.sqlstudio.client.panels.popups.PopUpException;
import org.bigsql.sqlstudio.client.panels.popups.ResultsPopUp;
import org.bigsql.sqlstudio.client.providers.ListProvider;

public class UtilityCommandAsyncCallback implements AsyncCallback<String> {

	private final DialogBox dialogBox;
	private final ListProvider dataProvider;
	private boolean autoRefresh = false;
	private boolean showResultOutput = false;
	
	public UtilityCommandAsyncCallback (DialogBox dialogBox, ListProvider dataProvider) {
		this.dialogBox = dialogBox;
		this.dataProvider = dataProvider;
	}
	
	public boolean isAutoRefresh() {
		return autoRefresh;
	}

	public void setAutoRefresh(boolean autoRefresh) {
		this.autoRefresh = autoRefresh;
	}

	public boolean isShowResultOutput() {
		return showResultOutput;
	}

	public void setShowResultOutput(boolean showResultOutput) {
		this.showResultOutput = showResultOutput;
	}

	@Override
	public void onFailure(Throwable caught) {
		ResultsPopUp pop = new ResultsPopUp(
				"Error", caught.getMessage());
		try {
			pop.getDialogBox();
		} catch (PopUpException ex) {
			Window.alert(ex.getMessage());
		}
		
		if (dialogBox != null)
			dialogBox.hide(true);
	}

	@Override
	public void onSuccess(String result) {
		JsArray<CommandJsObject> jArray = json2Messages(result);

		CommandJsObject res = jArray.get(0);
		if (res.getStatus() != null
				&& res.getStatus()
						.contains("ERROR")) {
			ResultsPopUp pop = new ResultsPopUp(
					"Error", res.getMessage());
			try {
				pop.getDialogBox();
			} catch (PopUpException caught) {
				Window.alert(caught.getMessage());
			}
		} else {
			if (showResultOutput) {
				ResultsPopUp pop = new ResultsPopUp("Results", res.getMessage());
				try {
					pop.getDialogBox();
				} catch (PopUpException caught) {
					Window.alert(caught.getMessage());
				}
			}
			
			if (autoRefresh)
				dataProvider.refresh();
			
			if (dialogBox != null)
				dialogBox.hide(true);
		}
	}

	private static final native JsArray<CommandJsObject> json2Messages(String json)
	/*-{ 
	  	return eval(json); 
	}-*/;

}
