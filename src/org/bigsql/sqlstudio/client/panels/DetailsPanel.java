/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.panels;

import org.bigsql.sqlstudio.client.models.ModelInfo;

public interface DetailsPanel {

	public void setItem(ModelInfo selectedItem);
	
	public void refresh();
	
}
